<?php
/**
 * The template for displaying the footer.
 *
 * @package OceanWP WordPress theme
 */ ?>

        </main><!-- #main -->

        <?php do_action( 'ocean_after_main' ); ?>

        <?php do_action( 'ocean_before_footer' ); ?>

        <?php
        // Elementor `footer` location
        if ( ! function_exists( 'elementor_theme_do_location' ) || ! elementor_theme_do_location( 'footer' ) ) { ?>

            <?php do_action( 'ocean_footer' ); ?>
            
        <?php } ?>

        <?php do_action( 'ocean_after_footer' ); ?>
                
    </div><!-- #wrap -->

    <?php do_action( 'ocean_after_wrap' ); ?>

</div><!-- #outer-wrap -->

<?php do_action( 'ocean_after_outer_wrap' ); ?>

<?php
// If is not sticky footer
if ( ! class_exists( 'Ocean_Sticky_Footer' ) ) {
    get_template_part( 'partials/scroll-top' );
} ?>

<?php
// Search overlay style
if ( 'overlay' == oceanwp_menu_search_style() ) {
    get_template_part( 'partials/header/search-overlay' );
} ?>

<?php
// If sidebar mobile menu style
if ( 'sidebar' == oceanwp_mobile_menu_style() ) {
    
    // Mobile panel close button
    if ( get_theme_mod( 'ocean_mobile_menu_close_btn', true ) ) {
        get_template_part( 'partials/mobile/mobile-sidr-close' );
    } ?>

    <?php
    // Mobile Menu (if defined)
    get_template_part( 'partials/mobile/mobile-nav' ); ?>

    <?php
    // Mobile search form
    if ( get_theme_mod( 'ocean_mobile_menu_search', true ) ) {
        get_template_part( 'partials/mobile/mobile-search' );
    }

} ?>
<marquee BEHAVIOR=alternate direction="left" onmouseover="stop" onmouseout="start"  height="10%">
	<img height="50" Width="80" src= "http://localhost/HBTravels/wp-content/uploads/2019/03/emirates.png">
	<img  height="50" Width="150" src= "http://localhost/HBTravels/wp-content/uploads/2019/03/uzbekistan.png">
	<img  height="50" Width="80" src= "http://localhost/HBTravels/wp-content/uploads/2019/03/singapore.png">
	<img  height="50" Width="150" src= "http://localhost/HBTravels/wp-content/uploads/2019/03/pakistan.png">
	<img  height="50" Width="150" src= "http://localhost/HBTravels/wp-content/uploads/2019/03/qatar.png">
	<img  height="50" Width="150" src= "http://localhost/HBTravels/wp-content/uploads/2019/03/malaysian.png">
	<img  height="50" Width="150" src= "http://localhost/HBTravels/wp-content/uploads/2019/03/sau.png">
	<img  height="50" Width="150" src= "http://localhost/HBTravels/wp-content/uploads/2019/03/philippine.png">
	<img  height="50" Width="150" src= "http://localhost/HBTravels/wp-content/uploads/2019/03/united.png">
</marquee>
<?php
// If full screen mobile menu style
if ( 'fullscreen' == oceanwp_mobile_menu_style() ) {
    get_template_part( 'partials/mobile/mobile-fullscreen' );
} ?>

<?php echo do_shortcode('[INSERT_ELEMENTOR id="165"]'); ?>
<?php wp_footer(); ?>
</body>
</html>